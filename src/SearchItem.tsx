import React, {PropsWithChildren} from "react";
import ItemComponentPropsInterface from "./interfaces/ItemComponentPropsInterface";
import styles from "./styles.module.scss";
 

export default function SearchItem(props: PropsWithChildren<ItemComponentPropsInterface>) {

  const {option, optionComponent, onClick} = props;
  const component = option.component || optionComponent;

  return <div className={styles.SearchItem} onClick={onClick}>
    {React.createElement(component, {...option})}
  </div>

}
