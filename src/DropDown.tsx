import React, {MutableRefObject, useRef, useState} from 'react';
import styles from "./styles.module.scss";
import DropItemInterface from "./interfaces/DropItemInterface";
import SearchItem from "./SearchItem";
import Hint from "./Hint";

import {useSmartDropDown} from "./SmartDropDownProvider";
import useOnClickOutside from "./hooks/useOnClickOutside";
import PrimaryKey from "./types/PrimaryKey";


const defaultHint = 'Ничего не найдено. Попробуйте выбрать другие ключевые слова.'

function DropDown() {

  const ref: MutableRefObject<any> = useRef();

  const {
    toggle, hide
  } = useSmartDropDown();

  useOnClickOutside(ref, () => hide());

  return <div className={styles.DropDown} ref={ref}>

      <div className={styles.ToggleButton} onClick={toggle}>
        <i className={'fa fa-plus'}/>
      </div>

      <SearchForm/>

    </div>

}


export default DropDown;


function SearchForm() {

  const {
    placeholder, availableOptions,
    onSelect, optionComponent
  } = useSmartDropDown();


  const [form, setForm] = useState({
    searchText: '',
    hint: null,
    placeholder: placeholder || "Поиск ..."
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {

    const searchText = event.target.value;

    setForm( (prevForm:any) => {
      return {
        ...prevForm,
        searchText
      }
    });

  }

  const handleClick = (pk: PrimaryKey) => {
    onSelect(pk);
  }

  const options = availableOptions.filter((option: DropItemInterface) => {
    return form.searchText.length < 1 || option.label
      .toLowerCase()
      .includes(form.searchText.toLowerCase());

  });

  return <div className={styles.Search}>

  <SearchArrow/>

  <div className={styles.SearchForm}>

    <input
      className={'form-control'}
      placeholder={form.placeholder}
      value={form.searchText}
      onChange={handleChange}
      type="text"
    />

  </div>

  <SearchItems
    optionComponent={optionComponent}
    items={options}
    onSelect={handleClick}
  />

  {options.length < 1 && <Hint hint={defaultHint}/>}

</div>
}

function SearchArrow() {
  return <div className={styles.Arrow}/>
}

function SearchItems({items, optionComponent, onSelect}: { items: DropItemInterface[], optionComponent?:any; onSelect: (pk: PrimaryKey) => void }) {

  return <div className={styles.SearchList}>
    {items.map((option: DropItemInterface, index: number) => {
      return <SearchItem
        key={`SearchItem_${index}`}
        optionComponent={optionComponent}
        option={option}
        onClick={() => onSelect(option.value)}
      />
    })}
  </div>

}
