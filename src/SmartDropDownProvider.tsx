import React, {ComponentProps, useContext, useState} from "react";
import DropItemInterface from "./interfaces/DropItemInterface";
import DefaultCard from "./cards/DefaultCard";
import PrimaryKey from "./types/PrimaryKey";

export type DropItemRecord = Record<PrimaryKey, DropItemInterface>;

const defaultLimit = 20;

const collectionMapToValue = (options: DropItemInterface[]): DropItemRecord => {
  const optionMapToValue: DropItemRecord = {};
  options.forEach((option: DropItemInterface) => {
    optionMapToValue[option.value] = option;
  });
  return optionMapToValue;
}

const SmartDropDownContext = React.createContext<any>({});

export function useSmartDropDown() {
  return useContext(SmartDropDownContext)
}

export function SmartDropDownProvider(props: ComponentProps<any>) {

  const {
    children,
    onChange,
    name,
    value,
    options,
    optionComponent,
    inputPlaceholder,
    toggleButtonTooltip,
    limit
  } = props;


  const selected:PrimaryKey[] = value ? (Array.isArray(value) ? value : [value]) : [];

  const [dropDown, setDropDown] = useState({
    isOpen: false,
    name,
    //selected,
    options,
    placeholder: inputPlaceholder,
    tooltip: toggleButtonTooltip,
  });


  /**
   * toggle isOpen
   */
  const toggle = () => {
    setDropDown(prevState => {
      return {...prevState, isOpen: !prevState.isOpen}
    })
  }

  const hide = () => {
    setDropDown(prevState => {
      return {...prevState, isOpen: false}
    })
  }


  /**
   * curd
   */
  const handleUnselect = (value: PrimaryKey) => {

    const nextSelected = [...selected].filter((pk: PrimaryKey, index: number) => pk !== value);
    onChange(name, nextSelected);

  }

  const handleSelect = (value: PrimaryKey) => {

    const nextSelected:PrimaryKey[] = [...selected, value];
    onChange(name, nextSelected);

  }

  const optionMapToValue: DropItemRecord = options ? collectionMapToValue(options) : {};

  const availableOptions = options && [...options].filter((option: DropItemInterface) => {
    if (selected.indexOf(option.value) === -1) {
      return true;
    }
  }).splice(0, (limit || defaultLimit) ) ;

  const params = {
    ...dropDown,
    optionMapToValue,

    toggle,
    hide,
    selected,
    availableOptions,
    optionComponent: optionComponent || DefaultCard,
    onSelect: handleSelect,
    onUnselect: handleUnselect,

  };

  return <SmartDropDownContext.Provider value={params}>
    {children}
  </SmartDropDownContext.Provider>
}
