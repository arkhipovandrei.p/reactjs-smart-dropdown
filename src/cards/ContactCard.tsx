import React from "react";

import styles from './ContactCard.module.scss';

export default function ContactCard(props:any)
{

    const {thumb, label, subLabel} = props;

    return <div className={styles.ContactCard}>
        {thumb && <span className={styles.ContactCardAvatar}>
            {thumb}
        </span>}
        <div className={styles.ContactCardDoubleDeck}>
            <div className={styles.ContactCardDoubleDeckTopSlot}>
                {label}
            </div>
            {subLabel && <div className={styles.ContactCardDoubleDeckBottomSlot}>
                {subLabel}
            </div>}

        </div>
    </div>
}
