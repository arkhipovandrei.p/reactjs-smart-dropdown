import React, {PropsWithChildren} from "react";
import styles from './styles.module.scss';
import DropDownInterface from "./interfaces/DropDownInterface";

import SelectedList from "./SelectedList";
import DropDown from "./DropDown";
import {SmartDropDownProvider, useSmartDropDown} from "./SmartDropDownProvider";


export default function SmartDropDown(props: PropsWithChildren<DropDownInterface>) {
  return <div>
    <SmartDropDownProvider {...props}>
      <SmartDropDownContent/>
    </SmartDropDownProvider>
  </div>
}


function SmartDropDownContent(props:any) {

  const {isOpen} = useSmartDropDown();

  const classes = [styles.SmartDropDown];

  if (isOpen) {
    classes.push(styles.isOpen);
  }

  return <div className={classes.join(' ')}>
    <SelectedList/>
    <DropDown/>
  </div>
}
