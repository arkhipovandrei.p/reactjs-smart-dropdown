import React, {PropsWithChildren} from "react";
import styles from "./styles.module.scss";
import {useSmartDropDown} from "./SmartDropDownProvider";



/**
 *
 * @param props
 * @constructor
 */
export default function SelectedList(props: PropsWithChildren<any>) {

  const {
    selected, optionMapToValue, onUnselect
  } = useSmartDropDown();

  if(!selected) {
    return null
  }

  return <div className={styles.SelectedList}>
    {
      selected.map((value: any) => {

          return <div className={styles.SelectedItem} key={`SelectedItem_${value}`}>

            <div className={styles.SelectedItemLabel}>
              {optionMapToValue[value].label}
            </div>

            <div className={styles.SelectedItemBtnRemove} onClick={() => onUnselect(value)}>
              <i className={'fa fa-remove'}/>
            </div>
          </div>
        }
      )
    }

  </div>

}
