import React, {PropsWithChildren} from "react";
import HintInterface from './interfaces/HintInterface';

import styles from "./styles.module.scss";

export default function Hint(props: PropsWithChildren<HintInterface>) {
  return <div className={styles.SearchHint}>
    {props.hint}
  </div>

}
