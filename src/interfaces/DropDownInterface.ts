import PrimaryKey from "../types/PrimaryKey";


export default interface DropDownInterface {
  name: string;
  options: any;
  onChange: (attribute: string, value: PrimaryKey[]) => void;
  value?: PrimaryKey | PrimaryKey[];
  plusButtonTooltip?: string;
  inputPlaceholder?: string;
  optionComponent?: any;
}
