import React from 'react';
import DropItemInterface from "./DropItemInterface";

export default interface ItemComponentPropsInterface {
  option: DropItemInterface;
  onClick: (event: React.MouseEvent) => void;
  optionComponent?: any
}
